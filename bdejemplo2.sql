﻿-- Ejemplo de creacion base de datos Ejercicio 1, hoja de ejercicios 1

DROP DATABASE IF EXISTS bdejemplo2;
CREATE DATABASE bdejemplo2;
USE bdejemplo2;


CREATE TABLE clientes(
  dni varchar(9),
  nombre varchar(10),
  apellidos varchar(20),
  fecha_nac date,
  tfno varchar(9),
  PRIMARY KEY (dni)
  );

CREATE TABLE productos(
  codigo varchar(5),
  nombre varchar(10),
  precio float,
  PRIMARY KEY (codigo)
);
CREATE TABLE proveedores(
  nif varchar(9),
  nombre varchar(10),
  direccion varchar(20),
  PRIMARY KEY (nif)
);

CREATE TABLE compran(
  dniCliente varchar(9),
  codigoProducto varchar(5),
  PRIMARY KEY (dniCliente, codigoProducto),
  CONSTRAINT  fkcomprancliente FOREIGN KEY (dniCliente) REFERENCES clientes (dni),
  CONSTRAINT fkcompranproducto FOREIGN KEY (codigoProducto) REFERENCES productos (codigo)
  );

CREATE TABLE suministran(
  codigoProducto varchar(5),
  nifProveedor varchar(9),
  PRIMARY KEY (codigoProducto, nifProveedor),
  UNIQUE KEY (codigoProducto),
  CONSTRAINT fksuministranproducto FOREIGN KEY (codigoProducto) REFERENCES productos (codigo),
  CONSTRAINT fksuministranproveedor FOREIGN KEY (nifProveedor) REFERENCES proveedores (nif)
  );


/* 
Insertamos datos en las tablas
*/

INSERT INTO clientes 
  VALUES ('123456789', 'Jose', 'Perez', '1999/01/01', '123456789');

INSERT INTO productos 
  VALUES ('12345', 'lavadora', 10.2);

INSERT INTO proveedores (nif, nombre, direccion)
  VALUES ('987654321','Maria','Calle 1');